Rails.application.routes.draw do
  root 'pages#index'

  resources :pages, only: [] do
    collection do
      get 'plot_line_series'
      get 'add_markers'
    end
  end
end
