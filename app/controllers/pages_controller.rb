require 'csv' 

class PagesController < ApplicationController
  def index
  end

  def nuvem
  end

  def plot_line_series
  	@data = {}
  	@publications = []
    @data[:categories] = categories
    @data[:data] = []
    
    CSV.open("#{Rails.root}/public/publicacoes_classificadas_positivas.csv") do |csv|
    	csv.each do |publication|
    		unless publication[0] == "id"
		    	@publications << publication
		    end
	    end
	    @publications.reverse!
    end

    @publications = @publications.group_by do |pub| 
    	Time.parse(pub[1]).strftime("%d")
    end

    @month_events = @publications.map { |i, v| v.length }

    @publications.each do |day, publications|
    	@publications[day] = publications.group_by do |pub|
    		Time.parse(pub[1]).strftime("%H")
    	end
    end

    ("01".."31").each do |i|
    	drilldown_data = []
    	("00".."23").each do |j|
    		drilldown_data << if @publications[i][j].nil?
	    		0
	    	else
	    		@publications[i][j].length
    		end
    	end
    	@data[:data] << {
    		y: @month_events[i.to_i-1],
        drilldown: {
	    		categories: day_categories,
          name: "Ocorrências dia #{i} de Agosto",
          data: drilldown_data
        }
    	}
    end

    render json: @data
  end

  def add_markers
    @publications = []
    CSV.open("#{Rails.root}/public/publicacoes_classificadas_positivas.csv") do |csv|
      csv.find_all do |publication|
        time = Time.parse(publication[1]) if publication[1] != "horario"
        time && time.day == params[:day].to_i && time.hour == params[:hour].to_i
      end.each do |publication| 
        publication[1] = Time.parse(publication[1]).strftime("%d/%m/%y às %H:%m")
        @publications << publication
      end
    end    
    render json: @publications
  end

  private
  def categories
  	[ '01/Ago', '02/Ago', '03/Ago', '04/Ago', '05/Ago', '06/Ago', '07/Ago', '08/Ago',
      '09/Ago', '10/Ago', '11/Ago', '12/Ago', '13/Ago', '14/Ago', '15/Ago', '16/Ago',
      '17/Ago', '18/Ago', '19/Ago', '20/Ago', '21/Ago', '22/Ago', '23/Ago', '24/Ago',
      '25/Ago', '26/Ago', '27/Ago', '28/Ago', '29/Ago', '30/Ago', '31/Ago']
  end

  def day_categories
    ["00h-01h","01h-02h","02h-03h","03h-04h","04h-05h","05h-06h","06h-07h",
      "07h-08h","08h-09h","09h-10h","10h-11h","11h-12h","12h-13h","13h-14h",
      "14h-15h","15h-16h","16h-17h","17h-18h","18h-19h","19h-20h","20h-21h",
      "21h-22h","22h-23h","23h-00h" ]
  end
end
