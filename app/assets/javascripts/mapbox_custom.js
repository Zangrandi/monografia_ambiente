function plotMap(){
	L.mapbox.accessToken = 'pk.eyJ1IjoiemFuZ3JhbmRpIiwiYSI6ImYyb3llQ2cifQ.XEbH2-0LEYafJCwGs_fiTA';
 	window.map = L.mapbox.map('map', 'zangrandi.qt2ffw7s').setView([-15.33630, -54.75430], 4);
	window.markers = new L.MarkerClusterGroup({
		showCoverageOnHover: false,
		disableClusteringAtZoom: 7
	});
}

function addMarkersToMap(day, hour){
  $.ajax({
    url: "/pages/add_markers" + "?day=" + day + "&hour=" + hour,
    dataType: "json",
    success: function(publications){
  		arrayLength = publications.length;
  		window.map.removeLayer(window.markers);
  		window.markers = new L.MarkerClusterGroup({
				showCoverageOnHover: false,
				disableClusteringAtZoom: 9
			});

  		for (var i = 0; i < arrayLength; i++) {
  			if(publications[i][4]) {
			    addMarker(publications[i]);
  			}
			}
    }
  });
}

function addMarker(publication) {
	L.mapbox.featureLayer({
	    // this feature is in the GeoJSON format: see geojson.org
	    // for the full specification
	    type: 'Feature',
	    geometry: {
	        type: 'Point',
	        // coordinates here are in longitude, latitude order because
	        // x, y is the standard for GeoJSON and many formats
	        coordinates: [
	          parseInt(publication[4]) + getRandomArbitrary(-0.1,0.1),
	          parseInt(publication[3]) + getRandomArbitrary(-0.1,0.1)
	        ]
	    },
	    properties: {
	        title: publication[1],
	        description: publication[2],
	        // one can customize markers by adding simplestyle properties
	        // https://www.mapbox.com/foundations/an-open-platform/#simplestyle
	        'marker-size': 'medium',
	        'marker-color': publication[3].split(".")[1].length > 8 ? '#42A6E7' : '#2084C5',
	        'marker-symbol': 'post'
	    }
	}).addTo(markers);

	window.map.addLayer(markers);
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}