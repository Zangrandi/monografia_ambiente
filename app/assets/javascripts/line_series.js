function plotLineSeries(){
  $.ajax({
    url: "/pages/plot_line_series",
    dataType: "json",
    success: function(controllerData){
      var chart;
      $(document).ready(function() {
          categories = controllerData.categories,
          name = 'Browser brands',
          data = controllerData.data;
      
          function setChart(name, categories, data) {
            chart.xAxis[0].setCategories(categories, false);
            chart.series[0].remove(false);
            chart.addSeries({
              name: name,
              data: data,
            }, false);
            chart.redraw();
          }
      
          chart = new Highcharts.Chart({
              colors: ['#1073B4'],
              chart: {
                  height: 580,
                  renderTo: 'graphic',
                  type: 'line',
                  backgroundColor:'transparent'
              },
              title: {
                  text: "Publicações no mês de agosto"
              },
              subtitle: {
                  text: 'Clique nos pontos para dar zoom'
              },
              legend:{
                  enabled:false
              },
              xAxis: {
                  categories: categories,
                  labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                      fontSize: '13px',
                      fontFamily: 'Verdana, sans-serif'
                    }
                  }
              },
              yAxis: {  
                  min: 0,
                  title: {
                      text: 'Nº de publicações'
                  }
              },
              plotOptions: {
                  line: {
                      cursor: 'pointer',
                      point: {
                          events: {
                              click: function() {
                                  var drilldown = this.drilldown;
                                  if (drilldown) { // drill down
                                      window.graphX = this.x + 1;
                                      setChart(drilldown.name, drilldown.categories, drilldown.data);
                                  } else { // restore
                                      $(document).scrollTo(630);
                                      addMarkersToMap(window.graphX, this.x);
                                      setChart(name, categories, data);
                                  }
                              }
                          }
                      },
                      dataLabels: {
                          enabled: true,
                          color: 'black',
                          style: {
                              fontWeight: 'bold'
                          },
                          formatter: function() {
                              return this.y;
                          }
                      }
                  }
              },
              tooltip: {
                  formatter: function() {
                      var point = this.point,
                          s = this.x +':<b>'+ this.y +' publicações</b><br/>';
                      if (point.drilldown) {
                          s += 'Clique para ver as publicações do dia '+ point.category;
                      } else {
                          s += 'Clique para visualizar as publicações no mapa';
                      }
                      return s;
                  }
              },
              series: [{
                  name: name,
                  data: data
              }],
              exporting: {
                  enabled: false
              }
          });
      });
    }
  });
}

